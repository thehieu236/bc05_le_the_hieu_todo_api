var renderTodoList = (todos) => {
	var contentHTML = ' ';

	todos.forEach((item) => {
    var contentTr = `
        <tr class ="border">
        <td class="text-center">${item.id}</td>
        <td>${item.name}</td>
        <td>${item.desc}</td>
        <td class="text-center">
        <input type="checkbox" class="checkbox w-8 h-8 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500" ${
					item.iscomplete ? (checked = 'checked') : ''
				} />
        </td>
        <td class="flex">
        <button onclick="editTodo(${
					item.id
				})" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l">
        Edit
        </button>
        <button onclick= "removeTodo(${
					item.id
				})" class="bg-gray-300 hover:bg-gray-400 border-r-4 text-gray-800 font-bold py-2 px-4 rounded-r fa fa-minus-square"></button>
        </td>
        </tr>
        `;
    

		contentHTML += contentTr;
	});

	document.getElementById('tr-content').innerHTML = contentHTML;
};

var turnOnLoading = () =>
	(document.getElementById('loading').style.display = 'block');

var turnOffLoading = () =>
	(document.getElementById('loading').style.display = 'none');

function layThongTinTuForm() {
	var yourTodo = document.getElementById('your__todo').value;
	var descTodo = document.getElementById('desc__todo').value;

	return {
		name: yourTodo,
		desc: descTodo,
	};
}
