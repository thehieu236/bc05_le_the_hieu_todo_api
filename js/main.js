const BASE__URL = 'https://635f4b14ca0fe3c21a991c87.mockapi.io';
var idEdited = null;
function fetchAllToDo() {
	turnOnLoading();
	axios({
		url: `${BASE__URL}/Todos`,
		method: 'GET',
	})
		.then((res) => {
			turnOffLoading();
			renderTodoList(res.data);
		})
		.catch((err) => {
			turnOffLoading();
			console.log('file: main.js ~ line 14 ~ fetchAllToDo ~ err', err);
		});
}

fetchAllToDo();

function removeTodo(idTodo) {
	turnOnLoading();
	axios({
		url: `${BASE__URL}/Todos/${idTodo}`,
		method: 'DELETE',
	})
		.then((res) => {
			turnOffLoading();
			fetchAllToDo();
			renderTodoList(res.data);
		})
		.catch((err) => {
			console.log('file: main.js ~ line 33 ~ err', err);
			turnOffLoading();
		});
}

function addTodo() {
	turnOnLoading();
	var data = layThongTinTuForm();
	var newTodo = {
		name: data.name,
		desc: data.desc,
		iscomplete: true,
	};
	axios({
		url: `${BASE__URL}/Todos`,
		method: 'POST',
		data: newTodo,
	})
		.then(() => {
			turnOffLoading();
			fetchAllToDo();
		})
		.catch((err) => {
			console.log('file: main.js ~ line 56 ~ err', err);
			turnOffLoading();
		});
}

function disableBtnUpdate(boolean, color) {
	var updateBtn = document.getElementById('update__btn');
	updateBtn.disabled = boolean;
	updateBtn.style.background = color;
}

disableBtnUpdate(true, '#3333');

function editTodo(idTodo) {
	turnOnLoading();
	axios({
		url: `${BASE__URL}/Todos/${idTodo}`,
		method: 'GET',
	})
		.then((res) => {
			turnOffLoading();
			disableBtnUpdate(false, '#38BDF8');

			document.getElementById('your__todo').value = res.data.name;
			document.getElementById('desc__todo').value = res.data.desc;
			idEdited = res.data.id;
		})
		.catch((err) => {
			console.log('file: main.js ~ line 84 ~ err', err);
		});
}

function updateTodo() {
	turnOnLoading();
	var data = layThongTinTuForm();
	axios({
		url: `${BASE__URL}/Todos/${idEdited}`,
		method: 'PUT',
		data: data,
	})
		.then(() => {
			turnOffLoading();
			fetchAllToDo();
		})
		.catch((err) => {
			console.log('file: main.js ~ line 101 ~ err', err);
			turnOffLoading();
		});
}
